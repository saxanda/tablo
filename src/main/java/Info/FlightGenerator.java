package Info;

import Entity.Airline;
import Entity.Airport;
import Entity.Flight;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class FlightGenerator {

    private final Random random = new Random();
    public int randomRange(int min, int max ){
        int range = max - min + 1;
        return (int)(Math.random()*range+1+min);
    }
    public LocalDateTime generateDateTime(int rangeFromNow){
        long lo = LocalDate.now().toEpochDay();
        long hi = LocalDate.now().plusDays(rangeFromNow).toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(lo,hi);
        LocalDateTime randomTime =LocalDateTime.of(LocalDate.ofEpochDay(randomDay), LocalTime.of(random.nextInt(24),(random.nextInt(60)/30)*30));
    return (randomTime.isAfter(LocalDateTime.now())) ? randomTime: randomTime.plusDays(1);
    }
    public Airline generateAirline(){return Airline.values()[random.nextInt(Airline.values().length)];}
//    public Airport generateAirport() {return Airport.values()[random.nextInt(Airport.values().length)];}
    public Airport generateAirport() {
        Airport[] airports = Airport.values();
        List<Airport> airportList = new ArrayList<>(Arrays.asList(airports));
        airportList.remove(Airport.BORYSPIL);
        return airportList.get(random.nextInt(airportList.size()));
    }
    public String generateDesignator(Airline airline){return airline.getCode() + randomRange(100,999);}

    public List<Flight> generateFlights(int howMany){
        List<Flight> flights = new ArrayList<>();
        String designator;
        int capacity;
        Airport from = Airport.BORYSPIL; // Завжди BORYSPIL
        LocalDateTime departure, arrival;
        for (int i = 0; i < howMany; i++) {
            Airline airline = generateAirline();
            designator = generateDesignator(airline);
            capacity = randomRange(150, 200) / 10 * 10;
            Airport to;
            do {
                to = generateAirport(); // Генеруємо випадковий аеропорт призначення, не BORYSPIL
            } while (to == from);

            flights.add(new Flight(i, designator, airline, capacity, from, to, generateDateTime(30)));
        }
        return flights;
    }


    public static void main(String[] args) {
    }
}

//        for (int i = 0; i < howMany; i++) {
//            airline = generateAirline();
//            designator = generateDesignator(airline);
//            capacity = randomRange(150,200)/10*10;
//            from = generateAirport();
//            do {to = generateAirport();} while (from.equals(to));
//            departure = generateDateTime(30);
//            flights.add(new Flight(i, designator,airline,capacity,from, to, departure));
//
//        }
