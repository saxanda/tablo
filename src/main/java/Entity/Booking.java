package Entity;

import java.time.LocalDateTime;

public class Booking {
    private int flightId;
    private String bookingId;
    private Airline airline;
    private int numberOfPassengers;
    private Airport origin;
    private Airport destination;
    private LocalDateTime departureTime;
    private String name;
    private String surname;

    public Booking(int flightId, Airline airline, int numberOfPassengers, Airport origin, Airport destination, LocalDateTime departureTime) {
        this.flightId = flightId;
        this.airline = airline;
        this.numberOfPassengers = numberOfPassengers;
        this.origin = origin;
        this.destination = destination;
        this.departureTime = departureTime;
    }

    public Booking(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Booking() {
    }

    @Override
    public String toString() {
        return "Booking " + bookingId;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public Airport getOrigin() {
        return origin;
    }

    public void setOrigin(Airport origin) {
        this.origin = origin;
    }

    public Airport getDestination() {
        return destination;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

}
