package Entity;

public enum Airport {
        BORYSPIL("KBP"),
        ZHULIANY("IEV"),
        HEATHROW("LHR"),
        BERLIN("BER"),
        DOHA("DOH"),
        NEW_YORK("JFK"),
        ISTANBUL("IST");
        private final String code;
        Airport(String code){this.code = code;}
        public String getCode(){return code;};

}
