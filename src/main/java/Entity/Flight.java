package Entity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class Flight implements Serializable {
    private int numberId;
    private String flightNumber;
    private Airline airline;
    private int maxNumSeats;
    private Airport origin;
    private Airport destination;
    private LocalDateTime departureTime;


    public Flight(int numberId, String flightNumber, Airline airline, int maxNumSeats, Airport origin, Airport destination, LocalDateTime departureTime){
    this.numberId = numberId;
    this.flightNumber = flightNumber;
    this.airline = airline;
    this.maxNumSeats = maxNumSeats;
    this.origin = origin;
    this.destination = destination;
    this.departureTime = departureTime;

}
    public Flight() {

    }

    public Flight(int numberId) {
        this.numberId = numberId;
    }

    public Flight(int numberId, String flightNumber, Airport origin, Airport destination, LocalDateTime departureTime) {
        this.numberId = numberId;
        this.flightNumber = flightNumber;
        this.origin = origin;
        this.destination = destination;
        this.departureTime = departureTime;
    }

    public void setNumberId(int numberId) {
        this.numberId = numberId;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public void setMaxNumSeats(int maxNumSeats) {
        this.maxNumSeats = maxNumSeats;
    }

    public void setOrigin(Airport origin) {
        this.origin = origin;
    }

    public void setDestination(Airport destination) {
        this.destination = destination;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public int getNumberId() { return numberId; }

    public String getFlightNumber() {
        return flightNumber;
    }

    public Airline getAirline() {
        return airline;
    }

    public int getMaxNumSeats() {
        return maxNumSeats;
    }

    public Airport getOrigin() {
        return origin;
    }

    public Airport getDestination() {
        return destination;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;

    }
    @Override
    public String toString() {
        return "Flight " + numberId;
    }


}
