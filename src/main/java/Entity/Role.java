package Entity;

public enum Role {
    USER("User"),
    GUEST("Guest");

    private final String name;

    public String getName() {
        return name;
    }

    Role(String name) {
        this.name = name;
    }
}
