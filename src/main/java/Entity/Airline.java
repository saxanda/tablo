package Entity;

public enum Airline {
    AMERICAN_AIRLINES("AA"),
    BRITISH_AIRWAYS("BA"),
    UKRAINE_INTERNATIONAL_AIRLINES("PS"),
    GERMAN_AIRWAYS("ZQ"),
    QATAR_AIRWAYS("QR"),
    TURKISH_AIRLINES("TK");
    private final String code;
    Airline(String code){this.code = code;}
    public String getCode(){return code;};
}
