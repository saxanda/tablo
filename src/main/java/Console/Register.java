package Console;

import Controllers.UserController;
import Entity.User;
import Entity.Sex;
import Services.UserService;

import static Entity.Role.USER;

public class Register implements Command{
    private UserService userService;
    private UserController userController;
    public void setUserService(UserService userService) {
        this.userService = userService;
    }


    @Override
    public String description() {
        return "Registration";
    }

    @Override
    public void execute() {
        ConsoleUtils consoleUtils = new ConsoleUtils();
        consoleUtils.displayMessage("User Registration");

        User user = new User();
        // Read user information from the console
        consoleUtils.displayMessage("Enter your name: ");
        user.setName(consoleUtils.readLine());

        consoleUtils.displayMessage("Enter your surname: ");
        user.setSurname(consoleUtils.readLine());

        consoleUtils.displayMessage("Enter your birth date (e.g., 1990-01-01): ");
        user.setBirthDate(Long.parseLong(consoleUtils.readLine()));

        consoleUtils.displayMessage("Enter your sex (MALE/FEMALE): ");
        try {
            user.setSex(Sex.valueOf(consoleUtils.readLine().toUpperCase()));
        } catch (IllegalArgumentException e) {
            consoleUtils.displayMessage("Invalid sex. Please enter MALE or FEMALE.");
            return; // Terminate registration
        }

        consoleUtils.displayMessage("Enter your login name: ");
        user.setLoginName(consoleUtils.readLine());

        consoleUtils.displayMessage("Enter your password: ");
        user.setPassword(consoleUtils.readPassword());
        user.setRole(USER); // Who registered received a role "USER" by default.

        // Now saving it to a database or a user list.
        //
        userService.registerUser(user);
        boolean registrationResult = userService.registerUser(user);

        if (registrationResult) {
            consoleUtils.displayMessage("User registration completed.");
        } else {
            consoleUtils.displayMessage("User with the same loginName already exists. Registration failed.");
        }

       user.displayUserInfo();


    }
}
