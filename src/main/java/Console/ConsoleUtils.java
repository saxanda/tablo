package Console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleUtils {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public ConsoleUtils() {
    }

    public static void pauseExecution() {
        System.out.print("Press Enter to Continue... ");
        try {
            reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean requestConfirmation() {
        while (true) {
            System.out.print("Confirm Operation (y/n)... ");
            try {
                String in = reader.readLine().toLowerCase();
                if (!in.equals("y") && !in.equals("yes")) {
                    if (!in.equals("n") && !in.equals("no")) {
                        continue;
                    }
                    return false;
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void displayMessage(String s) {
        System.out.println(s);
    }

    public String readLine() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String readPassword() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
