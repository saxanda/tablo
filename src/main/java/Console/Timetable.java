package Console;

import Entity.Flight;
import Info.FlightGenerator;
import Entity.Flight;
import DAO.FlightDAO;

import java.time.LocalDateTime;
import java.util.List;

public class Timetable implements Command  {
    private FlightDAO flightDAO;

    public Timetable(FlightDAO flightDAO) {
        this.flightDAO = flightDAO;
    }

     @Override
    public String description() {
        return "Airport Timetable";
    }

//    @Override
//    public void execute() {
//        final String PRINT_FORMAT = "| %-7s | %-10s | %-5s | %-30s | %8s |\n";
//        final String DASHES = new String(new char[76]).replace("\0", "-");
//        Flight f = new Flight(3);
//        System.out.println("number id"+f);
//        FlightGenerator t = new FlightGenerator();
//        t.generateFlights(3);
//        System.out.printf("%s\n", DASHES);
//    }

    @Override
    public void execute() {
        final String PRINT_FORMAT = "| %-7s | %-10s | %-5s | %-30s | %8s |\n";
        final String DASHES = new String(new char[76]).replace("\0", "-");

        // список рейсів зі зазначеними startTime і endTime
        List<Flight> flights = flightDAO.getAllFlightsFromKievWithin24Hours(LocalDateTime.now(), LocalDateTime.now().plusHours(24));

        System.out.printf("%s\n", DASHES);
        // кожен рейс у консоль
        for (Flight flight : flights) {
            System.out.printf(PRINT_FORMAT,
                    flight.getNumberId(),
                    flight.getFlightNumber(),
//                    flight.getAirline(),
//                    flight.getMaxNumSeats(),
                    flight.getOrigin(),
                    flight.getDestination(),
                    flight.getDepartureTime());
            System.out.printf("%s\n", DASHES);
        }
    }
}








