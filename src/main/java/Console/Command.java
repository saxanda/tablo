package Console;

public interface Command {
    String description();
    void execute();

}
