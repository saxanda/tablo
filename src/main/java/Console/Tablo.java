package Console;

import Controllers.FlightController;
import DAO.FlightDAO;
import DAO.UserDAO;
import Entity.Flight;
import Info.FlightGenerator;
import Services.FlightService;
import Services.UserService;
import java.io.FileNotFoundException;

import javax.swing.*;
import java.io.File;
import java.nio.file.Files;
import java.util.List;


public class Tablo {
    private FlightController flightController;
    private UserService userService; // Add a UserService field


    public Tablo(FlightController flightController, UserService userService) {
        this.flightController = flightController;
        this.userService = userService; // Initialize the UserService
        // Initialize the userDAO in the userService.
        UserDAO userDAO = new UserDAO(); // UserDAO initialization logic
        userService.setUserDAO(userDAO);
        // Initialize the flightController in the FlightsInfo.
        FlightsInfo flightsInfo = new FlightsInfo();
        flightsInfo.setFlightController(flightController);
    }
    // This method is used to initialize the required components.
    public static Tablo initialize() {

        FlightDAO flightDAO;

        if (fileExists("flights.ser")) {
            // Файл існує, завантажуємо рейси з нього
            flightDAO = new FlightDAO();
        } else {
            // Файл не існує, генеруємо та зберігаємо рейси
            flightDAO = generateAndSaveFlights();
        }

        FlightController flightController = new FlightController(new FlightService(flightDAO));
        UserService userService = new UserService();
        return new Tablo(flightController, userService);
    }

    private static FlightDAO generateAndSaveFlights() {
        FlightGenerator flightGenerator = new FlightGenerator();
        List<Flight> newFlights = flightGenerator.generateFlights(300);
        FlightDAO flightDAO = new FlightDAO();
        flightDAO.saveFlights(newFlights);
        return flightDAO;
    }

    private static boolean fileExists(String fileName) {
        File file = new File(fileName);
        return file.exists();
    }

    public void loginMenu() {
        Menu submenu = new Menu();
        submenu.setTitle("*** Sub Console.Menu A ***");
        submenu.addItem(new MenuItem("Option Aa"));
        submenu.addItem(new MenuItem("Option Bb"));
        submenu.execute();
    }
    public void registerMenu() {
        if (userService == null) {
            System.out.println("UserService is not properly initialized.");
            return;
        }
        Register r = new Register();
        Menu submenu = new Menu();
        submenu.setTitle(r.description());
        r.setUserService(userService); // Set the UserService for Register
        MenuItem proceedMenuItem = new MenuItem("Proceed with registration", r, "execute");
        submenu.addItem(proceedMenuItem);
        submenu.execute();
    }
    public void timetableMenu() {
//        Menu submenu = new Menu();
//        submenu.setTitle("*** Sub Console.Menu B ***");
//        submenu.execute();

//        Timetable t = new Timetable(flightController.getFlightService().getFlightDAO());
        Timetable t = new Timetable(flightController.getFlightService().getFlightDAO());

        t.execute();
//        MenuItem proceedMenuItem = new MenuItem("Proceed with timetable", t, "execute");
//        submenu.addItem(proceedMenuItem);
        //submenu.execute();

    }

    public void flightInfo() {
        Menu submenu = new Menu();
        FlightsInfo f = new FlightsInfo();
        f.setFlightController(flightController);
        submenu.setTitle("*** Flight Info ***");
        submenu.addItem(new MenuItem("Show Flights",f,"execute"));
        submenu.addItem(new MenuItem("Search a Flight by *ID*"));
        submenu.addItem(new MenuItem("Search a Flight by *Flight Number*"));
        submenu.execute();
    }
    public void myFlights() {
        Menu submenu = new Menu();
        submenu.setTitle("*** My Flights ***");
        submenu.addItem(new MenuItem("Show my Flights"));
        submenu.addItem(new MenuItem("Book a Flight"));
        submenu.addItem(new MenuItem("Cancel a Flight"));
        submenu.execute();
    }

   public void mainMenu(){
        Menu m = new Menu();
        m.setTitle("------------------Flight Booking Manager------------------------");
        m.addItem(new MenuItem("Timetable", this, "timetableMenu"));
        m.addItem(new MenuItem("Flight info", this, "flightInfo"));
        m.addItem(new MenuItem("My Flights", this, "myFlights"));
        m.addItem(new MenuItem("Register", this, "registerMenu"));
         // Airline timetable
        m.execute();
    }
}
