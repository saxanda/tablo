package Console;

import Controllers.FlightController;
import Controllers.UserController;
import Services.FlightService;

public class FlightsInfo implements Command {
    private FlightController flightController;

    public void setFlightController(FlightController flightController) {
        this.flightController = flightController;
    }

    @Override
    public String description() {
        return "Display Online Timetable";
    }

    @Override
    public void execute() {
//flightController.displayFlightDetails();
        flightController.displayOnlineTimetable();
    }
}
