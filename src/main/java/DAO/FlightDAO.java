package DAO;

import Entity.Flight;
import Info.FlightGenerator;
import Entity.Airport;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


public class FlightDAO {
    private List<Flight> flights;
    private FlightGenerator flightGenerator;

    public FlightDAO() {
        this.flights = loadFlights();
        if (this.flights.isEmpty()) {
            this.flightGenerator = new FlightGenerator();
            this.flights = flightGenerator.generateFlights(50);
            saveFlights(this.flights);
        }
    }

    public void saveFlights(List<Flight> flights) {
        try (FileOutputStream fos = new FileOutputStream("flights.ser");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(flights);
        } catch (FileNotFoundException e) {
            // Обробляємо виняток, коли файл не знайдено
            e.printStackTrace();
        } catch (IOException e) {
            // Обробляємо інші винятки, пов'язані з роботою з файлами
            e.printStackTrace();
        }
    }

    public List<Flight> loadFlights() {
        List<Flight> flights = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream("flights.ser");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            flights = (List<Flight>) ois.readObject();
        } catch (FileNotFoundException e) {
            // Якщо файл не знайдено, створюємо новий файл та повертаємо пустий список
            saveFlights(new ArrayList<>());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return flights;
    }

    public List<Flight> getAllFlightsFromKievWithin24Hours(LocalDateTime startTime, LocalDateTime endTime) {
        List<Flight> allFlights = loadFlights();
        List<Flight> flightsFromKievWithin24Hours = new ArrayList<>();

        for (Flight flight : allFlights) {
            if (flight.getOrigin() == Airport.BORYSPIL && flight.getDepartureTime().isAfter(startTime) && flight.getDepartureTime().isBefore(endTime)) {
                flightsFromKievWithin24Hours.add(flight);
            }
        }
        return flightsFromKievWithin24Hours;
    }

    public List<Flight> getAllFlights() {
        return flights; // Повертаємо список всіх рейсів
    }

    // Метод для встановлення макетованих рейсів для тестування
    public void setMockFlights(List<Flight> mockFlights) {
        this.flights = mockFlights;
    }

    public void addFlight(Flight flight) {
        flights.add(flight);
        saveFlights(flights);
    }
}


