package DAO;

import Entity.User;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements PersonDAO<User> {
    private List<User> users = new ArrayList<>();

    @Override
    public List<User> getAll() {
        return users;
    }

    @Override
    public User getByIndex(int index) {
        if (index >= 0 && index < users.size()) {
            return users.get(index);
        }
        return null;
    }

    @Override
    public void save(User user) {
        users.add(user);
    }

    @Override
    public boolean remove(int index) {
        if (index >= 0 && index < users.size()) {
            users.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(User user) {
        return users.remove(user);
    }

    @Override
    public void saveData(String filePath) {
        // Implement saving data to a file
    }

    @Override
    public void readData(String filePath) {
        // Implement reading data from a file
    }

    @Override
    public void loadData(List<User> persons) {
        users.addAll(persons);
    }
}