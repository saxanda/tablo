package DAO;

import Entity.Airport;
import Entity.Booking;
import Entity.Flight;

import java.io.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class BookingDAO {
    private FlightDAO flightDAO;


    public void saveBooking(List<Booking> booking) {
        try (FileOutputStream fos = new FileOutputStream("booking.ser");
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(booking);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Booking> loadBooking() {
        List<Booking> booking = new ArrayList<>();
        try (FileInputStream fis = new FileInputStream("booking.ser");
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            booking = (List<Booking>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return booking;
    }

    //Шаблон для знаходження потрібних рейсів
    public List<Flight> findAvailableFlights(Airport destination, LocalDateTime departureTime, int numberOfPassengers) {
        List<Flight> availableFlights = new ArrayList<>();

        List<Flight> allFlights = flightDAO.getAllFlightsFromKievWithin24Hours(LocalDateTime.now(), LocalDateTime.now().plusHours(24));

        for (Flight flight : allFlights) {
            if (flight.getDestination().equals(destination) &&
                    flight.getDepartureTime().isAfter(departureTime) &&
                    flight.getDepartureTime().isBefore(departureTime.plusHours(1))) {
                int availableSeats = flight.getMaxNumSeats();
                if (availableSeats >= numberOfPassengers) {
                    availableFlights.add(flight);
                }
            }
        }
        return availableFlights;
    }

}
