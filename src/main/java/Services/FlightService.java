package Services;

import Entity.Flight;
import DAO.FlightDAO;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FlightService {
    private FlightDAO flightDAO;

    public FlightService(FlightDAO flightDAO) {
        this.flightDAO = flightDAO;
    }

    public FlightDAO getFlightDAO() {
        return flightDAO;
    }

    // метод для отримання усіх рейсів з Києва впродовж 24 годин
    public List<Flight> getAllFlightsFromKievWithin24Hours() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime next24Hours = now.plusHours(24);
        return flightDAO.getAllFlightsFromKievWithin24Hours(now, next24Hours);
    }

    // метод для отримання рейсів по ID

    public Flight getFlightById(String flightId) {
        List<Flight> allFlights = flightDAO.getAllFlights();
        for (Flight flight : allFlights) {
            if (String.valueOf(flight.getNumberId()).equals(flightId)) {
                return flight;
            }
        }
        return null;
    }

    // Метод для встановлення макетованих рейсів для тестування
    public void setMockFlights(List<Flight> mockFlights) {
        flightDAO.setMockFlights(mockFlights);
    }

    public void addFlight(Flight flight) {
        flightDAO.addFlight(flight);
    }
}
