package Services;

import DAO.PersonDAO;
import DAO.UserDAO;
import Entity.User;


public class UserService {
    private PersonDAO<User> userDAO;



    public boolean registerUser(User user) {
        if (userDAO.getByIndex(0) != null) {
            return false; // Registration failed
        }

        userDAO.save(user);
        return true; // Registration successful
    }

    public void setUserDAO(PersonDAO<User> userDAO) {
        this.userDAO = userDAO;
    }

    // To be continue Other user-related services, such as login, update profile, etc.
}