package Services;

import DAO.BookingDAO;
import Entity.Airport;
import Entity.Booking;
import Entity.Flight;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BookingService {
    private Booking booking;
    private BookingDAO bookingDAO;
    Scanner scanner = new Scanner(System.in);


    public BookingService(BookingDAO bookingDAO) {
        this.bookingDAO = bookingDAO;
    }

    public BookingDAO getBookingDAO() {
        return bookingDAO;
    }

    //Заповнення параметрів длля знаходження потрібних рейсів
    public List<Flight> findAvailableFlights() {
        Scanner scanner = new Scanner(System.in);
        Airport destination = Airport.valueOf(scanner.next());
        LocalDateTime departureTime = LocalDateTime.parse(scanner.next());
        int numberOfPassengers = scanner.nextInt();
        scanner.close();
        return bookingDAO.findAvailableFlights(destination, departureTime, numberOfPassengers);
    }

    //Заповнення даних про пасажирів
    public List<Booking> getPassengerDetails(String numberId) {

        List<Booking> bookings = new ArrayList<>();

        for (int i = 0; i < booking.getNumberOfPassengers(); i++) {
            booking = new Booking();
            booking.setBookingId(numberId);

            System.out.println("Enter the name of Passenger " + (i + 1) + ":");
            String name = scanner.next();
            booking.setName(name);

            System.out.println("Enter the surname of Passenger " + (i + 1) + ":");
            String surname = scanner.next();
            booking.setSurname(surname);

            bookings.add(booking);
        }
        bookingDAO.saveBooking(bookings);
        scanner.close();
        return bookings;
    }

    //Скасування бронювання
    public String cancelReservation() {
        List<Booking> allBookings = bookingDAO.loadBooking();
        boolean bookingCancelled = false;

        System.out.println("Enter your booking ID:");
        String id = scanner.next();

        for (Booking booking : allBookings) {
            if (id.equals(booking.getBookingId())) {
                allBookings.remove(booking);
                bookingCancelled = true;
                break;
            }
        }
        scanner.close();
        if (bookingCancelled) {
            bookingDAO.saveBooking(allBookings);
            return "Booking with ID " + id + " has been cancelled.";
        } else {
            return "Booking with ID " + id + " was not found.";
        }
    }

}
