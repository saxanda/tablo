package Controllers;

import Entity.Booking;
import Entity.Flight;
import Services.BookingService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BookingController {
    public BookingService bookingService;
    private Flight flight;
    private Booking booking;
    Scanner scanner = new Scanner(System.in);

    public BookingController(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    public BookingService getBookingService() {
        return bookingService;
    }

    //Показ потрібних рейсів
    public void displayAvailableFlights() {
        List<Flight> availableflights = bookingService.findAvailableFlights();
        if (availableflights.isEmpty()) {
            System.out.println("No flights available for the specified destination, departure time, and number of passengers.");
        } else {
            for (Flight flight : availableflights) {
                System.out.println("Flight Number: " + flight.getFlightNumber());
                System.out.println("Airline: " + flight.getAirline());
                System.out.println("Max Number of Seats: " + flight.getMaxNumSeats());
                System.out.println("Origin Airport: " + flight.getOrigin());
                System.out.println("Destination Airport: " + flight.getDestination());
                System.out.println("Departure Time: " + flight.getDepartureTime());
                System.out.println("-------------------------------------------------------");
            }
            System.out.println("Enter the flight number to book or enter '0' to go back to the main menu:");
            int userInput = scanner.nextInt();
            scanner.close();
            if (userInput == 0) {
                //Повернення до головного меню
            } else if (flight.getFlightNumber().equals(userInput)) {
                //Бронювання одразу
                Flight selectedFlight = availableflights.get(userInput);
                bookingService.getPassengerDetails(selectedFlight.getFlightNumber());
            } else {
                System.err.println("Invalid input. Please enter a valid flight number or '0' to go back to the main menu.");
            }
        }
    }

    public void displayUserBookings(String name, String surname) {
        List<Booking> userBookings = new ArrayList<>();

        List<Booking> allBookings = bookingService.getBookingDAO().loadBooking();

        System.out.println("Bookings for " + name + " " + surname + ":");

        for (Booking booking : allBookings) {
            //Показ рейсів, що забронював користувач
            if (booking.getName().equals(name) && booking.getSurname().equals(surname)) {
                userBookings.add(booking);
                System.out.println("Booking ID: " + booking.getBookingId());
                System.out.println("Airline: " + booking.getAirline());
                System.out.println("Max Number of Seats: " + booking.getNumberOfPassengers());
                System.out.println("Origin Airport: " + booking.getOrigin());
                System.out.println("Destination Airport: " + booking.getDestination());
                System.out.println("Departure Time: " + booking.getDepartureTime());
                System.out.println("-------------------------------------------------------");
            } else {
                System.out.println("No bookings found for " + name + " " + surname);
            }
        }
    }

}
