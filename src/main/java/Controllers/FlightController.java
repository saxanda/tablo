package Controllers;

import Console.Tablo;
import Entity.Flight;
import Services.FlightService;
import java.util.List;
import java.util.Scanner;

public class FlightController {
    private FlightService flightService;
    public FlightService getFlightService() {
        return flightService;
    }

    public FlightController(FlightService flightService) {
        this.flightService = flightService;
    }

    // метод для відображення онлайн-табло рейсів
    public void displayOnlineTimetable() {
        List<Flight> flights = flightService.getAllFlightsFromKievWithin24Hours();
        if (flights.isEmpty()) {
            System.out.println("No flights from Kiev within the next 24 hours.");
        } else {
            for (Flight flight : flights) {
                System.out.println("Flight ID: " + flight.getNumberId());
                System.out.println("Flight Number: " + flight.getFlightNumber());
                System.out.println("Origin Airport: " + flight.getOrigin());
                System.out.println("Destination Airport: " + flight.getDestination());
                System.out.println("-------------------------------------------------------");
            }
        }
    }

    public void displayFlightDetails() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the flight ID: ");
        String flightId = scanner.next();
        Flight flight = flightService.getFlightById(flightId);
        if (flight != null) {
            System.out.println("Flight Number: " + flight.getFlightNumber());
            System.out.println("Origin Airport: " + flight.getOrigin());
            System.out.println("Destination Airport: " + flight.getDestination());
            System.out.println("Departure Time: " + flight.getDepartureTime());
        } else {
            System.out.println("Flight not found.");
        }
    }


}
