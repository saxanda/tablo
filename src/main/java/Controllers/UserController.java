package Controllers;

import Entity.User;

public class UserController {
    public void displayUserInfo(User user) {
        System.out.println("User Information:");
        System.out.println("Name: " + user.getName());
        System.out.println("Surname: " + user.getSurname());
        System.out.println("Birth Date: " + user.getBirthDate());
        System.out.println("Sex: " + user.getSex());
        System.out.println("Login Name: " + user.getLoginName());
        // Показати пароль на вибір
        System.out.println("Role: " + user.getRole());
    }
}
