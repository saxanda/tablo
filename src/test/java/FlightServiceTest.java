import Entity.Airport;
import Entity.Flight;
import DAO.FlightDAO;
import Services.FlightService;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FlightServiceTest {
    private FlightService flightService;
    private FlightDAO flightDAO;

    @Before
    public void setUp() {
        flightDAO = new FlightDAOMock();
        flightService = new FlightService(flightDAO);
    }

    @Test
    public void testGetAllFlightsFromKievWithin24Hours() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime next24Hours = now.plusHours(24);
        List<Flight> mockFlights = new ArrayList<>();
        Flight flight1 = new Flight();
        flight1.setOrigin(Airport.BORYSPIL);
        flight1.setDepartureTime(now.plusHours(1));
        mockFlights.add(flight1);
        Flight flight2 = new Flight();
        flight2.setOrigin(Airport.BORYSPIL);
        flight2.setDepartureTime(now.plusHours(3));
        mockFlights.add(flight2);
        FlightDAOMock flightDAOMock = (FlightDAOMock) flightDAO;
        flightDAOMock.setMockFlights(mockFlights);
        List<Flight> result = flightService.getAllFlightsFromKievWithin24Hours();
        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(flight1, result.get(0));
        assertEquals(flight2, result.get(1));
    }

    @Test
    public void testGetFlightById() {
        String flightId = "1";
        List<Flight> mockFlights = new ArrayList<>();
        Flight flight1 = new Flight();
        flight1.setNumberId(1);
        mockFlights.add(flight1);

        Flight flight2 = new Flight();
        flight2.setNumberId(2);
        mockFlights.add(flight2);

        FlightDAOMock flightDAOMock = (FlightDAOMock) flightDAO;
        flightDAOMock.setMockFlights(mockFlights);

        Flight result = flightService.getFlightById(flightId);

        assertNotNull(result);
        assertEquals(flight1, result);
    }
}

class FlightDAOMock extends FlightDAO {
    private List<Flight> mockFlights;

    public void setMockFlights(List<Flight> mockFlights) {
        this.mockFlights = mockFlights;
    }

    @Override
    public List<Flight> getAllFlightsFromKievWithin24Hours(LocalDateTime startTime, LocalDateTime endTime) {
        return mockFlights;
    }

    @Override
    public List<Flight> getAllFlights() {
        return mockFlights;
    }
}
