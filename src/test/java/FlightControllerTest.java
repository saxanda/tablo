import Controllers.FlightController;
import DAO.FlightDAO;
import Entity.Airport;
import Entity.Flight;
import Services.FlightService;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FlightControllerTest {
    private FlightController flightController;
    private FlightService flightService;
    private ByteArrayOutputStream outputStream;
    private ByteArrayInputStream inputStream;


    @Before
    public void setUp() {
        FlightDAO flightDAO = new FlightDAO(); // Створення об'єкту FlightDAO
        flightService = new FlightServiceMock(flightDAO); // Передача FlightDAO у конструктор FlightServiceMock
        flightController = new FlightController(flightService);
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));
        String input = "1\n";
        inputStream = new ByteArrayInputStream(input.getBytes());
        System.setIn(inputStream);
    }

    @Test
    public void testDisplayOnlineTimetableWithFlights() {
        List<Flight> mockFlights = new ArrayList<>();
        Flight flight1 = new Flight(1, "ABC123", Airport.BORYSPIL, Airport.HEATHROW, LocalDateTime.now().plusHours(1));
        mockFlights.add(flight1);
        flightService.setMockFlights(mockFlights);
        flightController.displayOnlineTimetable();

        String expectedOutput = "Flight ID: \\d+\n" +
                "Flight Number: ABC123\n" +
                "Origin Airport: BORYSPIL\n" +
                "Destination Airport: HEATHROW\n" +
                "-------------------------------------------------------\n";

        assertTrue(outputStream.toString().matches(expectedOutput));
    }


    @Test
    public void testDisplayFlightDetailsWithValidFlight() {
        LocalDateTime fixedTime = LocalDateTime.of(2023, 11, 10, 15, 43);
        Flight flight = new Flight(1, "QR573", Airport.BORYSPIL, Airport.NEW_YORK, fixedTime);
        flightService.setMockFlights(new ArrayList<>());
        flightService.addFlight(flight);
        flightController.displayFlightDetails();

        String expectedOutput = "Enter the flight ID: \n" +
                "Flight Number: QR573\n" +
                "Origin Airport: BORYSPIL\n" +
                "Destination Airport: NEW_YORK\n";

        // Визначення регулярного виразу для перевірки дати і часу, де \\d+ позначає будь-яку послідовність цифр
        String dateTimeRegex = "Departure Time: \\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}";

        // Перевірка, що рядок містить коректний формат дати і часу за допомогою регулярного виразу
        assertTrue(outputStream.toString().matches(expectedOutput + dateTimeRegex));
    }

    @Test
    public void testDisplayFlightDetailsWithInvalidFlight() {
        flightService.setMockFlights(new ArrayList<>());
        flightController.displayFlightDetails();
        String expectedOutput = "Enter the flight ID: \n" +
                "Flight not found.\n";
        assertEquals(expectedOutput, outputStream.toString());
    }
}



class FlightServiceMock extends FlightService {

    public FlightServiceMock(FlightDAO flightDAO) {
        super(flightDAO);
    }
    private List<Flight> mockFlights = new ArrayList<>();
    public void setMockFlights(List<Flight> flights) {
        mockFlights = flights;
    }

    public void addFlight(Flight flight) {
        mockFlights.add(flight);
    }

    @Override
    public List<Flight> getAllFlightsFromKievWithin24Hours() {
        return mockFlights;
    }
}
