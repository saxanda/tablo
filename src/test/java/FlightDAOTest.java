import DAO.FlightDAO;
import Entity.Airport;
import Entity.Flight;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.util.List;

public class FlightDAOTest {
    private FlightDAO flightDAO;

    @Before
    public void setUp() {
        flightDAO = new FlightDAO();
    }

    @Test
    public void testGetAllFlightsFromKievWithin24Hours() {
        LocalDateTime startTime = LocalDateTime.now();
        LocalDateTime endTime = startTime.plusHours(24);

        List<Flight> flights = flightDAO.getAllFlightsFromKievWithin24Hours(startTime, endTime);

        assertNotNull(flights);
        for (Flight flight : flights) {
            assertEquals(Airport.BORYSPIL, flight.getOrigin());
            assertTrue(flight.getDepartureTime().isAfter(startTime));
            assertTrue(flight.getDepartureTime().isBefore(endTime));
        }
    }

    @Test
    public void testGetAllFlights() {
        List<Flight> allFlights = flightDAO.getAllFlights();
        assertNotNull(allFlights);
    }
}
